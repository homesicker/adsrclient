package com.jegeren.adsrclient;

import com.jegeren.socket.SocketTransceiver;
import com.jegeren.socket.TCPClient;

import androidx.appcompat.app.AppCompatActivity;

import android.app.KeyguardManager;
import android.app.Service;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.Toast;
import android.os.Looper;
import android.os.Handler;
import android.os.StrictMode;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.content.SharedPreferences;
import android.widget.CompoundButton;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity implements OnClickListener, SensorEventListener {

    SharedPreferences sp;
    public Button btnConnect;
    public Switch swOne, swTwo;
    //public Button btnSend;
    public EditText edIP;
    private Handler handler = new Handler(Looper.getMainLooper());

    private SensorManager sensorManager;
    private Sensor accelerometer;

    private boolean allowSend = false;

    private float deltaX;
    private float deltaY;
    private float deltaZ;

    private float lastX;
    private float lastY;
    private float lastZ;

    public int clientNum = 1;
    private String savedIP;
    private String clientNumS;

    //public TextView dX;
    //public  TextView dY;
    //public TextView dZ;

    private TCPClient client = new TCPClient() {

        @Override
        public void onConnect(SocketTransceiver transceiver) {
            refreshUI(true);
        }

        @Override
        public void onDisconnect(SocketTransceiver transceiver) {
            refreshUI(false);
        }

        @Override
        public void onConnectFailed() {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, "Ошибка подключения",
                            Toast.LENGTH_SHORT).show();
                }
            });
        }

        @Override
        public void onReceive(SocketTransceiver transceiver, final String s) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(MainActivity.this, s,
                            Toast.LENGTH_SHORT).show();
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        setContentView(R.layout.activity_main);
        initializeView();
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        try {
            if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {

                accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
                sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);

            } else {
                Toast.makeText(this,"Акселерометр не обнаружен", Toast.LENGTH_SHORT).show();
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    public void initializeView(){
        //this.findViewById(R.id.btn_send).setOnClickListener(this);
        btnConnect = (Button) this.findViewById(R.id.connectBtn);
        btnConnect.setOnClickListener(this);
        //btnSend = (Button) this.findViewById(R.id.btn_send);
        //btnSend.setOnClickListener(this);
        swOne = (Switch)this.findViewById(R.id.clientOne);
        swTwo = (Switch)this.findViewById(R.id.clientTwo);

        swOne.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    clientNum = 1;
                    swTwo.setChecked(false);
                    //Toast.makeText(getApplicationContext(), "1", Toast.LENGTH_LONG).show();
                } else {
                    //Toast.makeText(getApplicationContext(), "Switch off!", Toast.LENGTH_LONG).show();
                }
            }
        });
        swTwo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    clientNum = 2;
                    swOne.setChecked(false);
                    //Toast.makeText(getApplicationContext(), "2", Toast.LENGTH_LONG).show();
                } else {
                    //Toast.makeText(getApplicationContext(), "Switch off!", Toast.LENGTH_LONG).show();
                }
            }
        });

        edIP = (EditText) this.findViewById(R.id.serverip);
        sp = getPreferences(MODE_PRIVATE);
        String savedIp = sp.getString("savedip", "192.168.1.1");
        int clientnum = sp.getInt("clientint", 1);
        if (clientnum == 1){
            swOne.setChecked(true);
        }
        if (clientnum == 2){
            swTwo.setChecked(true);
        }
        edIP.setText(savedIp);

        //edPort = (EditText) this.findViewById(R.id.ed_port);

        //dX = (TextView)this.findViewById(R.id.deltax);
        //dY = (TextView) this.findViewById(R.id.deltay);
        //dZ = (TextView) this.findViewById(R.id.deltaz);

        refreshUI(false);
    }

    //private void saveSwitch()

    //protected void onResume() {
        //super.onResume();
        //sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    //}

    //protected void onPause() {
        //super.onPause();
        //sensorManager.unregisterListener(this);
    //}

    //@Override
    //public void onStop() {
    //    client.disconnect();
    //    super.onStop();
    //}

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.connectBtn:
                connect();
                send();
                sp = getPreferences(MODE_PRIVATE);
                SharedPreferences.Editor ed = sp.edit();
                ed.putString("savedip", edIP.getText().toString());
                ed.putInt("clientint", clientNum);
                ed.apply();
                break;
        }
    }

    public void send(){
        if(allowSend){
            allowSend = false;
            //refreshUI2();
        }else
            allowSend = true;
            //refreshUI2();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        //displayCleanValues();
        displayCurrentValues();
        //displayMaxValues();

        deltaX = Math.abs(lastX - event.values[0]);
        deltaY = Math.abs(lastY - event.values[1]);
        deltaZ = Math.abs(lastZ - event.values[2]);

        if (deltaX < 2)
            deltaX = 0;
        if (deltaY < 2)
            deltaY = 0;
        if (deltaZ < 2)
            deltaZ = 0;

        lastX = event.values[0];
        lastY = event.values[1];
        lastZ = event.values[2];
    }

    public void displayCurrentValues() {
        //dX.setText(Float.toString(deltaX));
       // dY.setText(Float.toString(deltaY));
       // dZ.setText(Float.toString(deltaZ));
        if (allowSend && deltaX != 0 && deltaZ !=0){
            String data = createJSONData(deltaX, deltaY, deltaZ).toString() + ";";
            //Log.d("DATA", data);
            //sendStr(createJSONData(deltaX, deltaY, deltaZ).toString());
            try {
                client.getTransceiver().send(data);
            }catch (Exception e){
                //
            }

        }
    }

    public JSONObject createJSONData(float x, float y, float z) {
        JSONObject jsonData = new JSONObject();
        try {
            jsonData.put("X", String.valueOf((int)x));
            jsonData.put("Y", String.valueOf((int)y));
            jsonData.put("Z", String.valueOf((int)z));
            jsonData.put("Client", String.valueOf((int)clientNum));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonData;
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public  void refreshUI(final boolean isConnected) {
        handler.post(new Runnable() {
            @Override
            public void run() {
                //edPort.setEnabled(!isConnected);
                edIP.setEnabled(!isConnected);
                //btnSend.setEnabled(isConnected ? true : false);
                btnConnect.setText(isConnected ? "Отключиться" : "Подключиться");
                swOne.setEnabled(!isConnected);
                swTwo.setEnabled(!isConnected);
                btnConnect.setEnabled(!isConnected);
                View decorView = getWindow().getDecorView();
// Hide both the navigation bar and the status bar.
// SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
// a general rule, you should design your app to hide the status bar whenever you
// hide the navigation bar.
                int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
                decorView.setSystemUiVisibility(uiOptions);
            }
        });
    }

    @Override
    public void onAttachedToWindow() {
        //this.getWindow().setType(WindowManager.LayoutParams.TYPE_KEYGUARD_DIALOG);
        //super.onAttachedToWindow();
    }
    @Override
    public void onBackPressed() {
// super.onBackPressed();
// Not calling **super**, disables back button in current screen.
    }

 //   private void refreshUI2() {
 //       handler.post(new Runnable() {
 //           @Override
//            public void run() {
//                btnSend.setText(allowSend ? "Остановить отправку" : "Начать отправку");
//            }
//        });
//    }

    private void connect() {
        if (client.isConnected()) {
            client.disconnect();
        } else {
            try {
                Toast.makeText(this, "Попытка подключения", Toast.LENGTH_SHORT).show();
                String hostIP = edIP.getText().toString();
                int port = 7001;
                client.connect(hostIP, port);
            } catch (NumberFormatException e) {
                Toast.makeText(this, "Неверный IP", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
    }

    private void sendStr(String data) {
        try {
            //String data = edData.getText().toString();
            client.getTransceiver().send(data);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
